"""Methods for multiscale algorithms (resampling, etc)"""

def RefineScale(imList, vfList, scaleFactor):
    """
    Refine (move to finer scale) a list of images and vector fields

    This is usually used by Compute.Compute to transfer results (defined by the
    calling program) to use as inputs to the next scale
    """

    # TODO: resample imList, vfList

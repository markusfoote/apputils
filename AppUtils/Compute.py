"""Distributed computation convenience functions for use with PyCA"""

import Config

import os
import sys

try:
    from mpi4py import MPI
    HASMPI = True
except:
    HASMPI = False

# This is meant to be a general computation configuration spec
# Nothing in here should be specific to the algorithm being run
# The idea is that subsections conforming to this spec could be held in a
# central spot such as ~/.pyca/ and be included on the fly by command-line
# overrides such as
# ./Matching.py study.yaml computeConfig="~/.pyca/mpicluster.yaml"
# or by simply changing a single line within study.yaml

ComputeConfigSpec = {
    'useCUDA':
    Config.Param(default=False,
                 comment="Use GPU if available"),
    'interactive':
    Config.Param(default=False,
                 comment="Run in interactive mode (i.e. do plotting etc)"),
    'numProcesses':
    Config.Param(default=1,
                 comment="Number of MPI processes to spawn"),
    'hostList':
    Config.Param(default=['localhost'],
                 comment="List of hosts to use as MPI worker nodes")}


def Spawn(nproc=1, hostList=None):
    """Spawn mpiexec if not already done"""
    if not HASMPI:
        raise Exception("mpi4py required to spawn new MPI processes")
    # detect if we've started this process in MPI yet or do that now
    if not 'OMPI_COMM_WORLD_LOCAL_RANK' in os.environ:
        if hostList is None:
            hostList = ['localhost']

        print "Spawning " + str(nproc) + " MPI processes..."
        comm = MPI.COMM_SELF.Spawn(sys.executable,
                                    args=sys.argv, maxprocs=nproc)
        #CMD = "mpiexec -x PYTHONPATH -n " + str(nproc) + " --host \"" +\
              #','.join(hostList) + "\" " + ' '.join(sys.argv)
        #print(CMD)
        #os.system(CMD)
        #sys.exit(0)


def GetMPIInfo():
    """Just get the size, rank, name, localrank of MPI proces

    Note these are safe even when MPI is not running

    """
    if HASMPI:
        return {
            "size": MPI.COMM_WORLD.Get_size(),
            "rank": MPI.COMM_WORLD.Get_rank(),
            "name": MPI.Get_processor_name(),
            "local_rank": (int(os.environ['OMPI_COMM_WORLD_LOCAL_RANK']) if
                           'OMPI_COMM_WORLD_LOCAL_RANK' in os.environ.keys()
                           else 0)}
    else:
        return {
            "size": 1,
            "rank": 0,
            "name": '',
            "local_rank": 0}


def Compute(workerFn, cf, maxProcesses=0):
    """Run parallel workerFn using compute configuration"""
    if cf.compute.numProcesses > 1 and HASMPI:
        # number processes to spawn
        nproc = (min(cf.compute.numProcesses, maxProcesses) if
            maxProcesses > 0 else cf.compute.numProcesses)
        Spawn(nproc=nproc,
              hostList=cf.compute.hostList)

        # Simple check that we're not using more host processes than GPUs
        mpiLocalRank = GetMPIInfo()['local_rank']
        if cf.compute.useCUDA:
            if mpiLocalRank >= ca.GetNumberOfCUDADevices():
                raise Exception("Please don't use more host processes "+
                                "than GPUs")
            ca.SetCUDADevice(mpiLocalRank)

    # run the worker process
    return workerFn(cf)

def ConfigToMultiscale(cf):
    """
    Convert a single-scale config to multiscale format (with single scale 1)
    """
    cfmulti = Config._Object()
    cfmulti.__dict__['scales'] = [1]
    cfmulti.__dict__['paramsScale1'] = cf
    return cfmulti

def ComputeMultiscale(workerFn, upscaleParamsFn, cf, maxProcesses=0):
    """
    Run parallel workerFn using compute configuration

    Note that this version allows for multiscale runs if that is set in the
    configuration.

    Also note that a multiscale config file looks something like:

    scales: [4, 2, 1]
    paramsScale1: &fullscaleParams
        optim:
            Niter: 100
            stepSize: 0.1
        flow:
            sobolev:
                alpha: 0.1
                beta: 0.01
                sigma: 0.001
    paramsScale2:
        <<: *fullscaleParams
        optim:
            Niter: 500
            stepsize: 0.01
    paramsScale4:
        <<: *fullscaleParams
        optim:
            Niter: 1000
            stepsize: 0.001

    This will allow you to only override certain parameters within each
    downscaled run.
    """
    if not 'scales' in cf.__dict__:
        print "Detected single-scale config..."
        cfmulti = ConfigToMultiscale(cf)
        return ComputeMultiscale(workerFn, upscaleParamsFn, cfmulti, maxProcesses)

    if not isinstance(cf.scales, list):
        raise Exception('"scales" config option must be list of positive integers')

    cfCompute = cf.__dict__['paramsScale'+str(cf.scales[0])].compute
    if cfCompute.numProcesses > 1 and HASMPI:
        # number processes to spawn
        nproc = (min(cfCompute.numProcesses, maxProcesses) if
            maxProcesses > 0 else cfCompute.numProcesses)
        Spawn(nproc=nproc,
              hostList=cfCompute.hostList)

        # Simple check that we're not using more host processes than GPUs
        mpiLocalRank = GetMPIInfo()['local_rank']
        if cfCompute.useCUDA:
            if mpiLocalRank >= ca.GetNumberOfCUDADevices():
                raise Exception("Please don't use more host processes "+
                                "than GPUs")
            ca.SetCUDADevice(mpiLocalRank)

    # start with uninitialized parameters.  The transition function should
    # recognize this and initialize accordingly
    optimParams = None
    # ensure that we traverse scales in coarse to fine order
    cf.scales.sort(reverse=True)
    for (prevscale, nextscale) in zip([None]+cf.scales, cf.scales):
        cfnext = cf.__dict__['paramsScale'+str(nextscale)]
        optimParams = upscaleParamsFn(prevscale, nextscale, optimParams, cfnext)
        optimParams = workerFn(nextscale, optimParams, cfnext)

    return optimParams

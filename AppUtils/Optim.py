"""Generalized optimization configs and routines"""

import Config


def _valspec(_):
    """Hook to do extra validation"""
    pass
OptimConfigSpec = {
    'Niter':
    Config.Param(default=100,
                 comment="Number gradient descent iterations to run"),
    'stepSize':
    Config.Param(default=0.01,
                 comment="Gradient descent step size"),
    'maxPert':
    Config.Param(default=None,
                 comment="Maximum perturbation. Rough method of automatic " +
                 "step-size selection."),
    'method':
    Config.Param(default="FIXEDGD",
                 comment="Optimization scheme.  Only FIXEDGD is supported"),
    '_resource': "Optim",
    '_validation_hook': _valspec}


def LineSearchWolfe(f, x, E, g, p, a, c1, c2):
    """
    Do a backtracking Armijo line search terminating on Wolfe conditions

    fObj is the objective function
    fObjGrad returns the object and its gradient
    fStep takes a step
    fDot computes the inner product of two directions
    x is initial state
    E is the energy there.
    g is the gradient of fObj at x
    p is the search direction
    a is an initial estimate of the optimal stepsize
    """
    assert a > 0, "initial step size should be positive for wolfe conditions"
    assert c1 > 0, "c1 should be positive for wolfe conditions"
    assert c1 < c2, "c1 should be less than c2 for wolfe conditions"
    assert c2 < 1, "c2 should be less than 1 for wolfe conditions"
    

def Optimize(x, f, cfOptim):
    """
    Compute pure optimization iterations

    Return generator of (energy, state, other) pairs.

    Note that fStep should perform updates in place
    """
    step = cfOptim.stepSize
    for it in xrange(cfOptim.Niter):
        if it == 0:
            # initialization
            if cfOptim.method == 'SEARCHGD':
                p = f['Zero'](g)
            elif cfOptim.method == 'NCG':
                # TODO: initialize beta_i here
                pass
            elif cfOptim.method == 'RANDOMSEARCH':
                p = f['Zero'](g)
                Eprev = f['Obj'](x)
                a = cfOptim.stepSize  # initial gaussian variance
                cRej = 0.9  # contract on rejection
                cAcc = 1.1  # expand on acceptance

        if cfOptim.method == 'FIXEDGD':
            # compute objective and gradient
            (E, g, other) = f['ObjGrad'](x)

            if it == 0 and cfOptim.maxPert is not None and cfOptim.maxPert > 0.0:
                print "Doing maxPert adjustment"
                # get maximum size of gradient
                maxSizeGrad = f['MaxSizeGradient'](g)
                print "max gradient size is {}".format(maxSizeGrad)
                if maxSizeGrad > cfOptim.maxPert:
                    # set step to achieve maxPert
                    step *= cfOptim.maxPert/maxSizeGrad
                    print "new stepSize is {}".format(step)

            f['Step'](x, g, -step)

            yield (x, E, other)

        elif cfOptim.method == 'NCG':
            raise Exception('Nonlinear conjugate gradient not yet implemented')
        elif cfOptim.method == 'SEARCHGD':
            # search direction is negative gradient
            f['Copy'](p, g)
            f['MulC'](p, -1.0)
            # fixed decent values for line search
            (a, x) = LineSearchArmijo(f, x, E, g, p, a, 0.0001, 0.1)
        elif cfOptim.method == 'EVOLUTION':
            raise Exception('Random evolution-based optimization not yet implemented')
            f['Random'](p, a)  # random new p
            f['Step'](x, p, 1)  # step in that direction
            E = f['Obj'](x)  # evaluate new f(x)
            if E > Eprev:
                print "Energy increased, undoing step"
                f['Step'](x, p, -1)
                a *= cRej
            else:
                # acceptance
                a *= cAcc
                Eprev = E
                yield (x, E, other)

        else:
            raise Exception('Only fixed stepsize gradient descent supported')

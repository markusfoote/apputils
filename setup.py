#!/usr/bin/env python2
"""Setup script for AppUtils package"""

from distutils.core import setup

with open('README.txt') as file:
    # load the description from README so that bitbucket and PyPI pages match
    long_description = file.read()

    setup(name='AppUtils',
        version='0.01',
        description='Application utilities package',
        author='Jacob Hinkle',
        author_email='jacob@sci.utah.edu',
        url='http://bitbucket.org/jhinkle/apputils.git',
        packages=['AppUtils'],
        long_description=long_description)
